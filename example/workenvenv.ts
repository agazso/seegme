import { randomBytes } from 'crypto';

import { Work } from '../work';
import { Wallet } from '../wallet';
import { Envelope } from '../envelope';
import { ContactBase } from '../contact';
import { zeroHash } from '../types';



// simulated hash representing alice's last signature on some data
let aliceLastHash = randomBytes(32);


// simulated hash representing bob's last signature on some data
let bobLastHash = randomBytes(32);


// alice's wallet
let wallet = new Wallet();


// alice's work
let contactAlice = new ContactBase('alice', undefined);
let now = Math.floor(Date.now()/1000);
let work = new Work('what a dull boy', 42, now);


// envelope holding alice's signature on her work
let envelopeAlice = new Envelope('work', aliceLastHash);
envelopeAlice.setContact(contactAlice);
let workSerial = work.serialize();
envelopeAlice.write(workSerial);
envelopeAlice.digest();
envelopeAlice.sign(wallet);
let envelopeAliceJson = envelopeAlice.toJson();



// bob's wallet
let walletBob = new Wallet();


// bob unwraps alice's envelope and unpacks her details
let envelopeAliceRecovered = Envelope.fromJson(envelopeAliceJson);
let publicKeyAliceRecovered = envelopeAliceRecovered.recover();
let contactAliceRecovered = envelopeAliceRecovered.contact;


// bob unpack alice's work
let workSerialRecovered = envelopeAliceRecovered.content;
let workRecovered = Work.deserialize(workSerialRecovered);


// bob serializes alice's envelope 
let envelopeAliceSerial = envelopeAlice.serialize();


// then bob signs alice's signature envelope
let contactBob = new ContactBase('bob', undefined);
let envelopeBob = new Envelope('envelope', bobLastHash);
envelopeBob.setContact(contactBob);
envelopeBob.write(envelopeAliceSerial);
envelopeBob.digest();
envelopeBob.sign(walletBob);
let envelopeBobJson = envelopeBob.toJson();



// alice can unpack bob's signature and recover his public key
let envelopeBobRecovered = Envelope.fromJson(envelopeBobJson);
let publicKeyBobRecovered = envelopeBobRecovered.recover();
let contactBobRecovered = envelopeBobRecovered.contact;



// strut
console.log(contactBobRecovered.common_name + '\'s public key:', publicKeyBobRecovered);
console.log(contactAliceRecovered.common_name + '\'s public key:', publicKeyAliceRecovered);
console.log(contactAliceRecovered.common_name + '\'s work:', workRecovered.description, workRecovered.weight, workRecovered.timestamp);
