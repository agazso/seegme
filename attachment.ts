interface Attachment {
	mime:	string
	data:	any
}

export { Attachment };
