import { KeyHolder, Signature } from './keyholder';

let CONTACT_MAX_NAME = 255;

let limits = {
	'name':	CONTACT_MAX_NAME,
}


class ContactBase {

	common_name:	string		// common name of the contact
	meta_url:	string		// resource pointers to additional information registered for the contact

	constructor(common_name:string, meta_url?:string) {
		Contact._assertName(common_name);
		if (meta_url !== undefined) {
			Contact._assertUrl(meta_url);
		}
		this.common_name = common_name;
		this.meta_url = meta_url;
	}


	private static _assertName(name:string) {
		if (name.length > CONTACT_MAX_NAME) {
			throw('name too long');
		}
	}


	private static _assertUrl(url:string) {
	}
}

class Contact extends ContactBase implements KeyHolder {

	public_key:	Buffer		// public key of contact

	constructor(common_name:string, public_key:Buffer, meta_url?:string) {
		super(common_name, meta_url);
		Contact._assertPublicKey(public_key)
		this.public_key = public_key;
	}


	// TODO: untested
	public static fromContactBase(contactBase:ContactBase, public_key:Buffer): Contact {
		let contact = new Contact(contactBase.common_name, public_key, contactBase.meta_url);
		return contact
	}


	private static _assertPublicKey(key:Buffer) {
	}


	public verify(message:Buffer, signature:Signature): boolean {
		return true
	}

}


export { limits, Contact, ContactBase };
