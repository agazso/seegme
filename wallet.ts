import * as bip from 'bip39';
import { createLogger } from 'bunyan';
import * as secp256k1 from 'secp256k1';
import { Keccak } from 'sha3';
import { Signature } from './keyholder';



let logg = createLogger({
	name: 'wallet',
	stream: process.stderr,
	level: 'debug',
});


class Wallet {
	mnemonic: 	string
	key: 		Buffer

	constructor(mnemonic?: string) {

		// verify passed string, or generate new
		if (mnemonic === undefined || mnemonic == '') {
			mnemonic = bip.generateMnemonic(256);
			logg.debug('created new seed: ' + mnemonic);
		} else if (!bip.validateMnemonic(mnemonic)) {
			throw('invalid mnemonic "' + mnemonic + '"');
		}

		// create the key bytes and compress to 32 bytes
		let privKey = bip.mnemonicToSeedSync(mnemonic);
		let h = new Keccak(256);
		h.update(privKey);
		let privKeyHash = h.digest();

		// check validity of key for secp256k1
		if (!secp256k1.privateKeyVerify(privKeyHash)) {
			throw('invalid private key generated')
		}

		// everything verified, assign to object
		this.key = privKeyHash;
		this.mnemonic = mnemonic;
	}

	public signDigest(digest:Buffer): Signature {
		let a = new Uint8Array(digest);
		let sigObj = secp256k1.ecdsaSign(a, this.key);
		return {
		       signature: 	Buffer.from(sigObj.signature),
		       recid:		sigObj.recId,
		       };
	}
}

export { Wallet };
