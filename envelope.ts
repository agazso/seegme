import { Keccak } from 'sha3';
import * as secp256k1 from 'secp256k1';
import { createLogger } from 'bunyan';
import { Message, Type, Field } from 'protobufjs/light';

import { Signature } from './keyholder';
import { ContactBase } from './contact';


let logg = createLogger({
	name: 'wallet',
	stream: process.stderr,
	level: 'debug',
});


// maps to module names of valid types that the envelope can contain
// 'raw' can be used to define arbitrary data
// data integrity of serialized object from foreign modules is _not_ enforced here
// TODO: consider moving the content validation map to a different module

let VALID_TYPES_KEYS = [
	'raw',
	'envelope',
	'work',
]

let VALID_TYPES = {};
for (let i = 0; i < VALID_TYPES_KEYS.length; i++) {
	VALID_TYPES[VALID_TYPES_KEYS[i]] = Buffer.from([i]);
}


// passed to sign
// TODO: should perhaps instead be part of superclass for anything that needs to sign something
interface Signer {
	signDigest(digest:Buffer): Signature
}


interface EnvelopeJSON {
	typ:		string
	contact:	ContactBase
	content:	Buffer
	recid:		Buffer
	signature:	Buffer
	previousHash:	Buffer
	hash:		Buffer
}

let EnvelopeMsg = new Type('EnvelopeMsg');
EnvelopeMsg.add(new Field('typ', 1, 'bytes'));
EnvelopeMsg.add(new Field('content', 2, 'bytes'));
EnvelopeMsg.add(new Field('recid', 3, 'bytes'));
EnvelopeMsg.add(new Field('signature', 4, 'bytes'));
EnvelopeMsg.add(new Field('previousHash', 5, 'bytes'));
EnvelopeMsg.add(new Field('hash', 6, 'bytes'));


// represents signed data.
// can also sign other envelopes
export class Envelope {
	typ:		Buffer 		// type of object in content buffer
	contact:	ContactBase 	// identifies the signer
	content:	Buffer 		// content buffer
	recid:		Buffer 		// recovery id for signature
	signature:	Buffer 		// signature for content buffer
	previousHash:	Buffer 		// hash of previously signed envelope by signer (linked list)
	hash:		Buffer 		// proof to be signed, see digest method for details (this will be the previousHash for next signed envelope)


	constructor(typ:string, previousHash:Buffer) {
		if (VALID_TYPES[typ] === undefined) {
			throw ('type "' + typ + '" not in ' + Object.keys(VALID_TYPES).join(', '));
		}
		try {
			Envelope._assertHash(previousHash);
		} catch(e) {
			throw('invalid previousHash of length "' + previousHash +'"');
		}
		this.typ = VALID_TYPES[typ];
		this.previousHash = previousHash;
		this.contact = undefined;
		this.reset();
	}


	public setContact(contact:ContactBase) {
		this.contact = contact;
	}


	// prepares the envelope for new use for a previously defined type and previousHash
	public reset() {
		this.content = Buffer.alloc(0);
		this.signature = undefined;
		this.hash = undefined;
	}


	// append content to be digested.
	public write(b:Buffer): number {
		logg.debug('adding ' + b + ' to ' + this.content);
		this.content = Buffer.concat([this.content, b]);
		return this.content.length;
	}


	// digest is hash(content|previousHash).
	public digest(): boolean {
		let h = new Keccak(256);
		h.update(this.content);
		h.update(this.previousHash);
		this.hash = h.digest();
		return true;
	}


	// sign signs the digest created by Envelope.digest().
	public sign(wallet:Signer): boolean {
		if (this.hash === undefined) {
			throw('digest not ready for hashing');
		}

		let signatureObject = wallet.signDigest(this.hash);
		logg.debug('signature successful recid ' + signatureObject.recid + ' sig ' + signatureObject.signature);
		this.signature = signatureObject.signature;
		this.recid = Buffer.from([signatureObject.recid]);
		return true;
	}


	// recovers public key from data
	public recover(): Buffer {
		if (this.signature === undefined) {
			throw('no signature to recover');
		}
		let hashArray = new Uint8Array(this.hash);
		let signatureArray = new Uint8Array(this.signature);
		let public_key = secp256k1.ecdsaRecover(signatureArray, this.recid[0], hashArray);
		return Buffer.from(public_key);
	}


	public serialize(): Buffer {
		let msg = EnvelopeMsg.fromObject(this);
		let b = EnvelopeMsg.encode(msg).finish();
		return Buffer.from(b);
	}


	public static deserialize(b:Buffer): Envelope {
		let msg = EnvelopeMsg.decode(b);
		let o = EnvelopeMsg.toObject(msg);

		let e = new Envelope(VALID_TYPES_KEYS[o.typ[0]], o.previousHash);
		e.hash = o.hash;
		e.content = o.content;
		e.signature = o.signature;
		e.recid = o.recid;

		return e;
	}


	public toJson(): EnvelopeJSON {
		let j = Object.assign({}, this, {
			typ: VALID_TYPES_KEYS[this.typ[0]],
			contact: this.contact,
			content: this.content,
			recid: this.recid,
			signature: this.signature,
			previousHash: this.previousHash,
			hash: this.hash,
		});
		return j;
	}


	public static fromJson(s:EnvelopeJSON): Envelope {
		let envelope = new Envelope(s.typ, s.previousHash);
		Envelope._assertHash(s.hash);
		envelope.contact = s.contact;
		envelope.content = s.content;
		envelope.recid = s.recid;
		envelope.signature = s.signature;
		envelope.hash = s.hash;
		return envelope;
	}


	private static _assertHash(h:Buffer) {
		if (h.length < 32) {
			throw('previous hash must be 32 bytes');
		}
	}
}
