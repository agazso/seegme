interface KeyHolder {
	public_key:	Buffer
	verify(message:Buffer, signature:Signature): boolean
}


interface Signature {
	signature: 	Buffer
	recid:		number
};


export { KeyHolder, Signature };
