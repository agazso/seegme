import * as assert from 'assert';
import * as secp256k1 from 'secp256k1';

import { Envelope } from '../envelope';
import { Wallet } from '../wallet';
import { zeroHash } from '../types';
import { ContactBase } from '../contact';


describe('envelope', () => {
	it('create', function(done) {

		// zero-hash is a valid value and would irl mean
		// first signature ever for this key
		let previousHash = zeroHash; 
	

		// verify that input checker handles wrong type
		let envelope = undefined;
		try {
			envelope = new Envelope('foo', previousHash);
		} catch {
		}
		assert(envelope === undefined);


	        // create the envelope, add data to it and create the digest	
		envelope = new Envelope('raw', previousHash);
		let mockData = 'foo';
		let mockDataBuffer = Buffer.from(mockData);
		assert.equal(envelope.write(mockDataBuffer), mockData.length);
		assert(envelope.digest());


		// create wallet to sign the digest
		// TODO: use fixture to generate this wallet
		let w = new Wallet();
		let publicKeyBytes = secp256k1.publicKeyCreate(w.key);


		// perform the signature
		assert(envelope.sign(w));


		// recover public key from signature and compare to original public key
		let publicKeyRecoveredBuffer = envelope.recover();
		let publicKeyRecoveredBytes = new Uint8Array(publicKeyRecoveredBuffer);	
		assert.deepEqual(publicKeyBytes, publicKeyRecoveredBytes);

		done();
	});

	it('json', function(done) {
		let typ = 'raw';
		let previousHash = zeroHash;
		let contact = new ContactBase('Melvin Ferd', undefined);
		let ei = new Envelope(typ, previousHash);
		ei.setContact(contact);


		// TODO: use fixture to create an envelope complete with signed content
		// create the envelope, add data to it and create the digest	
		let mockData = 'foo';
		let mockDataBuffer = Buffer.from(mockData);
		ei.write(mockDataBuffer);
		ei.digest();

		let w = new Wallet();


		// perform the signature
		assert(ei.sign(w));


		let j = ei.toJson();
		let eo = Envelope.fromJson(j);

		assert.deepEqual(eo, ei);

		done();
	});

	it('protobuf', function(done) {
		let typ = 'raw';
		let previousHash = zeroHash;
		let ei = new Envelope(typ, previousHash);


		// TODO: use fixture to create an envelope complete with signed content
		// create the envelope, add data to it and create the digest	
		let mockData = 'foo';
		let mockDataBuffer = Buffer.from(mockData);
		ei.write(mockDataBuffer);
		ei.digest();

		let w = new Wallet();


		// perform the signature
		assert(ei.sign(w));

		let msgBuf = ei.serialize();

		let eo = Envelope.deserialize(msgBuf);
		assert.deepEqual(eo, ei);

		done();
	});
});
