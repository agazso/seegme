import { Wallet } from '../wallet';
import * as assert from 'assert';
import { Keccak } from 'sha3';


describe('wallet', () => {
	it('create', function(done) {
		let w = new Wallet();
		assert(w !== undefined);

		let poo;
		try {
			w = new Wallet('foo bar baz');
		} catch(e) {
			poo = true;
		}

		assert(poo);
		done();
	});


	// TODO: move wallet creation to fixture, make deterministic
	it('sign buffer data', function(done) {
		let w = new Wallet();

		let plainText = 'foo';
		let plainTextBuf = Buffer.from(plainText)
		let h = new Keccak(256);
		h.update(plainText);
		let digestBuffer = h.digest();

		done();
	});
});
