import { Work, limits } from '../work';
import { ContactBase } from '../contact';
import * as assert from 'assert';


describe('work', () => {
	it('create', function(done) {
		let fail = false;
		let w = undefined;
		let description = 'and no play ';

		// input checking catches negative weight
		try {
			w = new Work(description, -1, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

		// input checking catches zero weight
		w = undefined;
		try {
			w = new Work(description, 0, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

		// input passes invalid values
		w = new Work(description, 1, limits.timestamp);
		assert.notEqual(w, undefined);

		w = new Work(description, limits.weight, limits.timestamp);
		assert.notEqual(w, undefined);

		// input checking catches too height weigth
		w = undefined;
		try {
			w = new Work(description, limits.weight + 1, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

		// input checking catches too low timestamp
		w = undefined;
		try {
			w = new Work(description, limits.weight, limits.timestamp - 1);
		} catch {
		}
		assert.equal(w, undefined);
		
		// input checking catches too long name
		w = undefined;
		for (; description.length < limits.description + 1; description += description);
		try {
			w = new Work(description, 42, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

		done();
	});

	it('json', function(done) {
		let description = 'and no play ';
		let weight = 42;
		let timestamp = limits.timestamp;
		let wi = new Work(description, weight, timestamp);

		// export to json has valid values
		let jo = wi.toJson();
		assert(jo.description == description);
		assert(jo.weight == weight);

		description += description;
		weight += weight;
		let ji = {
			description: description,
			weight: weight,
			timestamp: timestamp,
		};

		// import from json has valid values
		let wo = Work.fromJson(ji);
		assert.equal(wo.description, description);
		assert.equal(wo.weight, weight);
		assert.equal(wo.timestamp, timestamp);

		done();
	});


	it('protobuf', function(done) {
		let description = 'and no play ';
		let weight = 42;
		let timestamp = limits.timestamp;
		let wi = new Work(description, weight, timestamp);

		//let msgBuf = wi.serialize(wi);
		let msgBuf = wi.serialize();

		let wo = Work.deserialize(msgBuf);

		assert.deepEqual(wo, wi);
		done();
	});
});
